<?php

require('connection.php');

$stmt = $dbh->prepare('UPDATE `user` SET `firstname` = :firstname, `lastname` = :lastname WHERE `id` = :id');
$stmt->bindParam(':id', $id, PDO::PARAM_INT);
$stmt->bindParam(':firstname', $firstname);
$stmt->bindParam(':lastname', $lastname);

$id = 1;
$firstname = 'Константин';
$lastname = 'Сидоров';
