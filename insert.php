<?php

require('connection.php');

$stmt = $dbh->prepare('INSERT INTO user (firstname, lastname) VALUES (:firstname, :lastname)');
$stmt->bindParam(':firstname', $firstname);
$stmt->bindParam(':lastname', $lastname);

$firstname = 'Никита';
$lastname = 'Сидоров';
$stmt->execute();

$firstname = 'Владимир';
$lastname = 'Соколов';
$stmt->execute();
