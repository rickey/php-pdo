<?php

require('connection.php');

$views = 500;
$sth = $dbh->prepare('SELECT art.title, art.content, art.views
    FROM article AS art
    WHERE art.views > :views;');

$sth->bindParam(':views', $views, PDO::PARAM_INT);

// PDOStatement::fetchAll
$sth->execute();
$result = $sth->fetchAll();
print_r($result);

// PDO::FETCH_ASSOC
$sth->execute();
$result = $sth->fetchAll(PDO::FETCH_ASSOC);
print_r($result);

// PDOStatement::fetch and PDO::FETCH_OBJ
$sth->execute();
while($item = $sth->fetch(PDO::FETCH_OBJ)) {
    print_r($item);
}
